<a name="readme-top"></a>

<h3 align="center">ERPLY Middleware test project</h3>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

### Built With

* [Go lang]

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started

This is a middleware for ERPLY API, which can be used to create and to get customers through the ERPLY API.

Added documentation with gin-swagger by URL localhost:PORT/docs/index.html#/ 

### Prerequisites

* Go 1.20

You can install Go CLI from [Downloads].

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/zeus-arval/erplymiddleware.git
   ```
2. Run API


   If you have Macos or Linux:
   ```
   make run
   ```
   If you have Windows without with Command Line:
   ```
   go run cmd/erplymiddleware/main.go
   ```
<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTACT -->
## Contact

Your Name - [Artur Valdna](https://www.linkedin.com/in/artur-valdna/) - arturvaldna@gmail.com

Project Link: [https://gitlab.com/zeus-arval/erplymiddleware](ERPLY Middleware)

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- MARKDOWN LINKS & IMAGES -->
[Go lang]: https://go.dev
[Downloads]: https://go.dev/dl/
