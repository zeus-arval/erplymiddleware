package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/zeus-arval/erplymiddleware/config"
	_ "gitlab.com/zeus-arval/erplymiddleware/docs"
	"gitlab.com/zeus-arval/erplymiddleware/internal/db"
	"gitlab.com/zeus-arval/erplymiddleware/internal/transport/rest"
)

// @title 	Erply Customer API
// @version 1.0
// @description Test project of middleware for customers' handling
// @host 	localhost:5432
// @BasePath /api/v1
func main() {
	var c config.Config

	configUrl := "./config/envs"
	err := config.LoadConfig(&c, configUrl)

	if err != nil {
		fmt.Printf("Error occured during the config reading, %s", err)
	}

	database, err := db.Init(c.DbUrl)
	db.AddTable(database, db.ClientSession{})

	if err != nil {
		fmt.Printf("Error occured during the database initialization, %s", err)
	}

	router := gin.Default()
	controller.RegisterRoutes(router, database)
	router.Run(c.Port)
}
