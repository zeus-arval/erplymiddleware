ERPLY_MIDDLEWARE=cmd/erplymiddleware/
REPOSITORY=./internal/repository/
MODELS=internal/models/
SERVICE=./internal/service/
REST=./internal/transport/rest/
BINARY_NAME=gitlab.com/zeus-arval/erplymiddleware/
DATABASE=./internal/db/
CONFIG=./config/

build:
	GOARCH=amd64 GOOS=darwin go build -o ${BINARY_NAME}-darwin $(ERPLY_MIDDLEWARE)main.go
	GOARCH=amd64 GOOS=linux go build -o ${BINARY_NAME}-linux $(ERPLY_MIDDLEWARE)main.go
	GOARCH=amd64 GOOS=windows go build -o ${BINARY_NAME}-windows $(ERPLY_MIDDLEWARE)main.go

run:
	go mod tidy
	go get -u -v all
	make build
	go run $(ERPLY_MIDDLEWARE)main.go

unit_test:
	echo "Starting unit tests"
	go test $(DATABASE)
	go test $(SERVICE)models
	go test $(REPOSITORY)auth
	go test $(CONFIG)
	echo "Finishing unit tests"
