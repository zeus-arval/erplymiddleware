package service

import (
	"fmt"
	"github.com/magiconair/properties/assert"
	"testing"
	_ "testing"
)

func TestMapToDictionary(t *testing.T) {
	testTable := []struct {
		object   any
		expected map[string]string
	}{
		{
			object: struct {
				FirstName   string `json:"firstName"`
				LastName    string `json:"lastName"`
				PhoneNumber string `json:"phoneNumber"`
			}{
				FirstName:   "John",
				LastName:    "Doe",
				PhoneNumber: "55555555",
			},
			expected: map[string]string{
				"firstName":   "John",
				"lastName":    "Doe",
				"phoneNumber": "55555555",
			},
		},
		{
			object: struct {
				FirstName   string `json:"firstName"`
				LastName    string `json:"lastName"`
				PhoneNumber string `json:"phoneNumber"`
			}{},
			expected: map[string]string{
				"firstName":   "",
				"lastName":    "",
				"phoneNumber": "",
			},
		},
	}

	for index, testingData := range testTable {
		t.Logf("Calling MapToDictionary with %d case -> expected: %s", index+1, testingData.expected)

		data, _ := MapToDictionary(testingData.object)

		assert.Equal(t, data, testingData.expected,
			fmt.Sprintf("Incorrect result, Expected %s, got %s",
				data, testingData.expected))
	}
}
