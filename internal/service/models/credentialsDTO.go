package service

type CredentialsDTO struct {
	ClientCode string `json:"clientCode"`
	SessionKey string `json:"sessionKey"`
}
