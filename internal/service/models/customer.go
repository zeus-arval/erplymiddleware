package service

type (
	Customer struct {
		FullName  string `json:"fullName"`
		FirstName string `json:"firstName"`
		LastName  string `json:"lastName"`
		Phone     string `json:"phone"`
		Email     string `json:"email"`
		Birthday  string `json:"birthday"`
		Username  string `json:"webshopUsername"`
	}
)
