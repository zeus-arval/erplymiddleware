package service

import (
	"encoding/json"
)

func MapToDictionary(object any) (dictionary map[string]string, err error) {
	data, err := json.Marshal(object)
	if err != nil {
		return map[string]string{}, err
	}
	err = json.Unmarshal(data, &dictionary)
	return dictionary, err
}
