package authService

import (
	"github.com/erply/api-go-wrapper/pkg/api"
	"gitlab.com/zeus-arval/erplymiddleware/internal/db"
	"gitlab.com/zeus-arval/erplymiddleware/internal/repository"
	credentialsService "gitlab.com/zeus-arval/erplymiddleware/internal/service/models"
	"log"
)

type (
	CurrentClient struct {
		Client     *api.Client
		sessionKey string
	}
	UserCredentials struct {
		Username   string `json:"username"`
		Password   string `json:"password"`
		ClientCode string `json:"clientCode"`
	}
)

func InitializeClient(user *UserCredentials) (*api.Client, error) {
	log.Printf("%s %s %s\n", user.Username, user.Password, user.ClientCode)
	return api.NewClientFromCredentials(user.Username, user.Password, user.ClientCode, nil)
}

func InitializeClientWithSessionKey(credentials *credentialsService.CredentialsDTO) (*api.Client, error) {
	return api.NewClient(credentials.SessionKey, credentials.ClientCode, nil)
}

func GetSessionKey(cli *api.Client) (sessionKey string, err error) {
	sessionKey, err = cli.GetSession()
	return sessionKey, err
}

func GetCredentials(repo *repository.Repository, sessionKey string) db.ClientSession {
	return repo.AuthRepo.GetSession(sessionKey)
}

func SaveSession(session *db.ClientSession, repo *repository.Repository) {
	repo.AuthRepo.AddSession(session)
}
