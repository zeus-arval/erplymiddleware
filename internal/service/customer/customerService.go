package customerService

import (
	"context"
	"github.com/erply/api-go-wrapper/pkg/api"
	"github.com/erply/api-go-wrapper/pkg/api/customers"
	"gitlab.com/zeus-arval/erplymiddleware/internal/repository"
	authService "gitlab.com/zeus-arval/erplymiddleware/internal/service/login"
	credentialsService "gitlab.com/zeus-arval/erplymiddleware/internal/service/models"
)

func AddCustomer(clientCode string, token string, customer *credentialsService.Customer) (customers.CustomerImportReport, error) {
	cli, err := getClient(clientCode, token)

	if err != nil {
		return customers.CustomerImportReport{}, err
	}

	customerData, err := mapCustomer(*customer)
	importReport, err := cli.CustomerManager.SaveCustomer(context.Background(), customerData)

	if err != nil {
		return customers.CustomerImportReport{}, err
	}

	return *importReport, err
}

func getClient(clientCode string, token string) (*api.Client, error) {
	return api.NewClient(token, clientCode, nil)
}

func mapCustomer(customer credentialsService.Customer) (customerData map[string]string, err error) {
	return credentialsService.MapToDictionary(customer)
}

func GetCustomers(clientCode string, sessionKey string, repo *repository.Repository) (customers []customers.Customer, err error) {
	cli, err := authService.InitializeClientWithSessionKey(&credentialsService.CredentialsDTO{ClientCode: clientCode, SessionKey: sessionKey})

	if err != nil {
		return nil, err
	}

	customers, err = cli.CustomerManager.GetCustomers(context.Background(), map[string]string{})

	if err != nil {
		return nil, err
	}

	return customers, err
}
