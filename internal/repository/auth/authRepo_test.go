package authRepo

import (
	"fmt"
	"github.com/magiconair/properties/assert"
	database "gitlab.com/zeus-arval/erplymiddleware/internal/db"
	databaseHandler "gitlab.com/zeus-arval/erplymiddleware/internal/db/test"
	"log"
	"testing"
)

var dbUrl = "./test/test_database.db"

func TestAuthenticationRepository_AddSession(t *testing.T) {
	var actualSession database.ClientSession
	expectedSession := database.ClientSession{
		SessionKey: "test",
		ClientCode: "99",
	}

	t.Logf("Starting testing DB Init()")

	repo := initializeAuthenticationRepository()
	repo.AddSession(&expectedSession)
	repo.DB.First(&actualSession)

	assert.Equal(t, actualSession.SessionKey, expectedSession.SessionKey,
		fmt.Sprintf("\nExpected sessionKey: [%s]\nActual sessionKey: [%s]",
			expectedSession.SessionKey, actualSession.SessionKey))

	assert.Equal(t, actualSession.ClientCode, expectedSession.ClientCode,
		fmt.Sprintf("\nExpected clientCode: [%s]\nActual clientCode: [%s]",
			expectedSession.ClientCode, actualSession.ClientCode))

	databaseHandler.ClearDbIfExists(dbUrl)
}

func initializeAuthenticationRepository() AuthenticationRepository {
	db, err := database.Init(dbUrl)

	if err != nil {
		log.Fatalln(err.Error())
	}

	err = database.AddTable(db, database.ClientSession{})

	if err != nil {
		log.Fatalln(err.Error())
	}

	return AuthenticationRepository{DB: db}
}
