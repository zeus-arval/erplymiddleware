package authRepo

import (
	"fmt"
	"gitlab.com/zeus-arval/erplymiddleware/internal/db"
	"gorm.io/gorm"
)

type (
	AuthenticationRepository struct {
		DB *gorm.DB
	}
)

func (repository AuthenticationRepository) AddSession(session *db.ClientSession) {
	deleteSessionIfNewExists(repository, session)
	repository.DB.Create(&session)
}

func deleteSessionIfNewExists(repository AuthenticationRepository, session *db.ClientSession) {
	var user db.ClientSession
	repository.DB.Table("client_sessions").Where("client_code = ?", session.ClientCode).First(&user)

	if user.SessionKey != "" && user.ClientCode != "" {
		repository.DB.Unscoped().Delete(user)
	}
}

func (repository AuthenticationRepository) GetSession(sessionKey string) db.ClientSession {
	var dbToken db.ClientSession
	repository.DB.First(&dbToken, "session_key = ?", sessionKey)

	fmt.Printf("dbToken %s", dbToken.ClientCode)
	return dbToken
}
