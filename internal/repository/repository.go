package repository

import (
	authRepo "gitlab.com/zeus-arval/erplymiddleware/internal/repository/auth"
	"gorm.io/gorm"
)

type Repository struct {
	AuthRepo *authRepo.AuthenticationRepository
}

func Init(db *gorm.DB) *Repository {
	authRepo := authRepo.AuthenticationRepository{DB: db}

	return &Repository{
		AuthRepo: &authRepo,
	}
}
