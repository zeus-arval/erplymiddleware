package controller

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/zeus-arval/erplymiddleware/docs"
	"gitlab.com/zeus-arval/erplymiddleware/internal/repository"
	"gorm.io/gorm"
)

type (
	Handler struct {
		Repo *repository.Repository
	}
)

// @BasePath /api/v1

func RegisterRoutes(router *gin.Engine, db *gorm.DB) {
	h := &Handler{
		Repo: repository.Init(db),
	}

	router.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	apiRoutes := router.Group("/api/v1")

	customerRoutes := apiRoutes.Group("/customers")
	customerRoutes.POST("/", h.AddCustomer)
	customerRoutes.GET("/", h.GetCustomers)

	apiRoutes.POST("/login", h.Login)
}
