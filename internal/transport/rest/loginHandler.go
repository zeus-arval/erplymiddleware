package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	_ "gitlab.com/zeus-arval/erplymiddleware/docs"
	"gitlab.com/zeus-arval/erplymiddleware/internal/db"
	authService "gitlab.com/zeus-arval/erplymiddleware/internal/service/login"
	"net/http"
)

type (
	UserSession struct {
		ClientCode string `header:"clientCode"`
		SessionKey string `header:"sessionKey"`
	}
)

// Login 			godoc
// @Summary 		Login
// @Description 	Gets user credentials and make a request to the Erply API. If there is a user with such credentials, returns sessionKey.
// @Tags			login
// @Param			userCredentials body authService.UserCredentials true "User credentials"
// @Produce			application/json
// @Success			200 {string} sessionKey
// @Failure			404 {string} error
// @Failure			400 {string} error
// @Router			/login [post]
func (h Handler) Login(ctx *gin.Context) {
	var user authService.UserCredentials
	var err error

	if err := ctx.BindJSON(&user); err != nil {
		fmt.Printf("Error occured: %s", err.Error())
		err = ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}
	fmt.Printf("Initializing the client, %s %s %s", user.Username, user.Password, user.ClientCode)

	client, err := authService.InitializeClient(&user)

	if err != nil {
		fmt.Printf("Not Found: %s", err.Error())
		err = ctx.AbortWithError(http.StatusNotFound, err)
		return
	}

	sessionKey, err := authService.GetSessionKey(client)
	clientToken := db.ClientSession{SessionKey: sessionKey, ClientCode: user.ClientCode}

	fmt.Printf("%s\n", sessionKey)
	fmt.Printf("%s %s\n", clientToken.SessionKey, clientToken.ClientCode)

	authService.SaveSession(&clientToken, h.Repo)

	if err != nil {
		err = ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"sessionKey": sessionKey,
	})
}
