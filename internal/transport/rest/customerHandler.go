package controller

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	_ "gitlab.com/zeus-arval/erplymiddleware/docs"
	customerService "gitlab.com/zeus-arval/erplymiddleware/internal/service/customer"
	authService "gitlab.com/zeus-arval/erplymiddleware/internal/service/login"
	service "gitlab.com/zeus-arval/erplymiddleware/internal/service/models"
	"net/http"
)

// AddCustomer 		godoc
// @Summary 		Adds customer
// @Description		Adds customer to the Erply Database through Erply API
// @Tags			customers
// @Param			Customer body service.Customer true "Customer"
// @Param			SessionKey query string true "Session Key"
// @Produce			application/json
// @Success			200 {string} token
// @Failure			400 {object} string
// @Failure			404 {object} string
// @Failure			500 {object} string
// @Router			/customers [post]
func (h Handler) AddCustomer(ctx *gin.Context) {
	var customer service.Customer

	if err := ctx.BindJSON(&customer); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	sessionKey := ctx.Query("SessionKey")

	credentials := authService.GetCredentials(h.Repo, sessionKey)

	fmt.Println(credentials.SessionKey, credentials.ClientCode)

	report, err := customerService.AddCustomer(credentials.ClientCode, credentials.SessionKey, &customer)

	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ctx.JSON(http.StatusCreated, report)
}

// GetCustomers 	godoc
// @Summary 		Gets all customers
// @Description		Gets all customers by a certain client code from the Erply Database through Erply API
// @Tags			customers
// @Param			SessionKey query string true "Session Key"
// @Produce			application/json
// @Success			200 {string} token
// @Failure			404 {object} string
// @Failure			400 {object} string
// @Failure			500 {object} string
// @Router			/customers [get]
func (h Handler) GetCustomers(ctx *gin.Context) {
	sessionKey := ctx.Query("SessionKey")

	if sessionKey == "" {
		ctx.AbortWithError(http.StatusUnauthorized, errors.New("session key is empty"))
		return
	}
	fmt.Printf("sessionKey: %s", sessionKey)

	credentials := authService.GetCredentials(h.Repo, sessionKey)
	fmt.Println(credentials.ClientCode)
	customers, err := customerService.GetCustomers(credentials.ClientCode, credentials.SessionKey, h.Repo)

	if err != nil {
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	if len(customers) == 0 {
		ctx.BindJSON(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, customers)
}
