package databaseHandler

import (
	"os"
)

func ClearDbIfExists(dbUrl string) {
	if _, err := os.Stat(dbUrl); err == nil {
		os.RemoveAll(dbUrl)
	}
}
