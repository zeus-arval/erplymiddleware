package db

import (
	"fmt"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"os"
)

type ClientSession struct {
	gorm.Model
	SessionKey string `gorm:"sessionKey"`
	ClientCode string `gorm:"clientCode"`
}

func Init(dbUrl string) (*gorm.DB, error) {
	db, err := gorm.Open(sqlite.Open(dbUrl), &gorm.Config{})

	if err != nil {
		fmt.Printf("database doesn't exist")
		os.Create(dbUrl)
		db, err = gorm.Open(sqlite.Open(dbUrl), &gorm.Config{})
		fmt.Printf("created database")
	}
	return db, err
}

func AddTable(db *gorm.DB, data any) error {
	return db.AutoMigrate(data)
}
