package db

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	databaseHandler "gitlab.com/zeus-arval/erplymiddleware/internal/db/test"
	"log"
	"testing"
)

var dbUrl = "./test/test_database.db"

type TestPerson struct {
	FirstName string `gorm:"first_name"`
	LastName  string `gorm:"last_name"`
}

type EmptyStruct struct {
	Field string `gorm:"field"`
}

func TestInit(t *testing.T) {
	databaseHandler.ClearDbIfExists(dbUrl)
	t.Logf("Starting testing DB Init()")

	Init(dbUrl)

	if assert.FileExists(t, dbUrl, fmt.Sprintf("Database %s was not created", dbUrl)) {
		databaseHandler.ClearDbIfExists(dbUrl)
	}
}

func TestAddTables(t *testing.T) {
	databaseHandler.ClearDbIfExists(dbUrl)
	db, err := Init(dbUrl)
	var empty EmptyStruct
	var testPerson TestPerson

	if err != nil {
		log.Fatalln(err.Error())
	}

	dbPerson := TestPerson{LastName: "Doe", FirstName: "John"}

	err = AddTable(db, TestPerson{})
	if err != nil {
		log.Fatalln(err.Error())
	}

	db.Create(dbPerson)
	db.First(&testPerson)
	db.First(&empty)

	assert.Equal(t, "", empty.Field)
	assert.Equal(t, dbPerson, testPerson)
	databaseHandler.ClearDbIfExists(dbUrl)
}
