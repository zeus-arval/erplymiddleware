package config_test

import (
	"fmt"
	"github.com/magiconair/properties/assert"
	"gitlab.com/zeus-arval/erplymiddleware/config"
	"log"
	"testing"
)

func TestLoadConfig(t *testing.T) {
	var c config.Config
	t.Logf("Calling LoadConfig")

	port, dbUrl := ":9999", "./test"
	configUrl := "./test"

	err := config.LoadConfig(&c, configUrl)

	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, c.Port, port, fmt.Sprintf("\nExpected port: %s\nActual port: %s", port, c.Port))
	assert.Equal(t, c.DbUrl, dbUrl, fmt.Sprintf("\nExpected db url: %s\nActual db url: %s", dbUrl, c.DbUrl))
}
