package config

import (
	"github.com/spf13/viper"
	"log"
)

type Config struct {
	Port  string `mapstructure:"PORT"`
	DbUrl string `mapstructure:"DB_URL"`
}

func LoadConfig(c *Config, configUrl string) error {
	viper.AddConfigPath(configUrl)
	viper.SetConfigName("app")
	viper.SetConfigType("env")
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		log.Printf("Error reading env file, %s", err)
		return err
	}

	if err := viper.Unmarshal(&c); err != nil {
		log.Println(err)
		return err
	}
	return nil
}
